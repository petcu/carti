const search = document.getElementById('search');
const matchList = document.getElementById('match-list');
const button_search = document.getElementById('button-search');

const link_json = "./json/books.json";

async function getBooksList() {
    const response = await axios.get(link_json);
    return response.data;
}

const searchBooks = async searchText => {
    const books = await getBooksList();
    const fuse = new Fuse(books, {
        keys: ['title']
    });

    let matches = fuse.search(searchText).slice(0, 10);
    if (searchText.length === 0)
        matches = [];

    outputHtml(matches);
}

button_search.onclick = () => searchBooks(search.value);

const outputHtml = matches => {
    if (matches.length > 0) {
        const html = `<div class="list-group">` +
            matches.map(match => `
                <a href="${match.item.link}" target="_blank" class="list-group-item list-group-item-action">
                    ${match.item.title}
                </a>
            `).join('') + `</div>`;

        matchList.innerHTML = html;
    }
};

search.addEventListener('keypress', function(e) {
    if(e.key === "Enter")
        searchBooks(search.value);
});
